set(SRC_FILES	
	common/Application.cpp
	common/Camera.cpp
	common/Mesh.cpp
	common/ShaderProgram.cpp
	common/DebugOutput.cpp
	main.cpp
)

set(HEADER_FILES
	common/Application.hpp
	common/Camera.hpp
	common/Mesh.hpp
	common/ShaderProgram.hpp
    common/DebugOutput.h
)

set(SHADER_FILES
    492OsipovData/shader.vert
    492OsipovData/shader.frag
)

source_group("Shaders" FILES
    ${SHADER_FILES}
)

include_directories(common)

MAKE_TASK(492Osipov 1 "${SRC_FILES}")

