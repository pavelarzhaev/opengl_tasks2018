SET(APP_NAME 491Kotelnikova)

SET(SRC_FILES    
    Main.cpp
    Application.cpp
    Mesh.cpp
    ShaderProgram.cpp
)

SET(HEADER_FILES
    Application.h
    Mesh.h
    ShaderProgram.h
)

SET(SHADER_FILES
    491KotelnikovaData/shader.vert
    491KotelnikovaData/shader.frag
)

SOURCE_GROUP("Shaders" FILES	
	${SHADER_FILES}	
)

MAKE_TASK(${APP_NAME} 1 "${SRC_FILES}")

INSTALL(DIRECTORY ${APP_NAME}Data DESTINATION ${CMAKE_INSTALL_PREFIX})