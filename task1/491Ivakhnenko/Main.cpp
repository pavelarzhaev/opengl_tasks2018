#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>

// Лента Мёбиуса
// https://ru.wikipedia.org/wiki/Лента_Мёбиуса
// http://virtualmathmuseum.org/Surface/moebius_strip/moebius_strip.html

const double aa = 0.5;

class Mebius : public Application
{
public:
    MeshPtr _mebius;

    ShaderProgramPtr _shader;

    float _periodicity = 50.0;

private:

    // x = aa * (cos(v) + u * cos(v / 2) * cos(v))
    // y = aa * (sin(v) + u * cos(v / 2) * sin(v))
    // z = aa * u * sin(v / 2)
    // -1 <= u <= 1
    // 0 <= v < 2 * Pi

    double getX( const double u, const double v ) {
        return aa * (cos(v) + u * cos(v / 2) * cos(v));
    }

    double getY( const double u, const double v ) {
        return aa * (sin(v) + u * cos(v / 2) * sin(v));
    }

    double getZ( const double u, const double v ) {
        return aa * u * sin(v / 2);
    }

    glm::vec3 getCoors( const double u, const double v ) {
        return glm::vec3( getX( u, v ), getY( u, v ), getZ( u, v ) );
    }

    glm::vec3 getNormals( const double u, const double v) {
        double n_x = u/2 * sin(v) - sin(v/2) * cos(v) * (1 + u * cos(v/2));
        double n_y = u/2 * cos(v) + sin(v/2) * sin(v) * (1 + u * cos(v/2));
        double n_z = cos(v/2) * (1 + u * cos(v/2));

        return glm::normalize( glm::vec3( n_x, n_y, n_z ) );
    }

public:
    MeshPtr makeMebius(float radius, unsigned int M) {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        for( unsigned int i = 0; i < M; ++i ) {
            double u1 = -1. + i * 2 / M;
            double u2 = -1. + (i + 1) * 2 / M;

            for( unsigned int j = 0; j < M; ++j ) {
                double v1 = 2. * (double)glm::pi<float>() * j / M;
                double v2 = 2. * (double)glm::pi<float>() * (j + 1) / M;

                //Первый треугольник, образующий квадрат

                vertices.push_back( getCoors( u1, v1 ) );
                vertices.push_back( getCoors( u1, v2 ) );
                vertices.push_back( getCoors( u2, v1 ) );

                normals.push_back( getNormals( u1, v1 ) );
                normals.push_back( getNormals( u1, v2 ) );
                normals.push_back( getNormals( u2, v1 ) );

                //Второй треугольник, образующий квадрат

                vertices.push_back( getCoors( u2, v2 ) );
                vertices.push_back( getCoors( u1, v2 ) );
                vertices.push_back( getCoors( u2, v1 ) );

                normals.push_back( getNormals( u2, v2 ) );
                normals.push_back( getNormals( u1, v2 ) );
                normals.push_back( getNormals( u2, v1 ) );
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _mebius = makeMebius(0.5f, static_cast<unsigned int>(_periodicity));

        _shader = std::make_shared<ShaderProgram>("491IvakhnenkoData/shader.vert", "491IvakhnenkoData/shader.frag");
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;

        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            _periodicity -= 100.0f * dt;
            _periodicity = std::max(4.0f, _periodicity);
            std::cout << _periodicity << std::endl;
            _mebius = makeMebius(0.5f, static_cast<unsigned int>(_periodicity));
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            _periodicity += 100.0f * dt;
            _periodicity = std::min(100.0f, _periodicity);
            std::cout << _periodicity << std::endl;
            _mebius = makeMebius(0.5f, static_cast<unsigned int>(_periodicity));
        }
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _mebius->modelMatrix());
        _mebius->draw();
    }
};

int main()
{
    Mebius app;
    app.start();

    return 0;
}