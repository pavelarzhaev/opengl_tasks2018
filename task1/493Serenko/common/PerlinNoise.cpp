#include "PerlinNoise.h"

PerlinNoise::PerlinNoise(unsigned int num_of_octaves, float persistence, int seed) :
	numOctaves(num_of_octaves),
	persistence(persistence) {
	permutationTable = std::vector<int>(1024);
	srand(seed);
	for (unsigned int i = 0; i < 1024; ++i) {
		permutationTable[i] = rand() % 1337;
	}
}

float PerlinNoise::QunticCurveInterpolation(float a, float b, float x) {
	float t = x * x * x * (x * (x * 6 - 15) + 10);
	return a  + (b - a) * t;
}

Vector PerlinNoise::GetPseudoRandomGradientVector(int x, int y) {
	int v = (int)(((x * 1836311903) ^ (y * 2971215073) + 4807526976) & 1023);
	v = permutationTable[v] & 3;
	switch (v) {
	case 0: return Vector(1.f, 0.f);
	case 1: return Vector(-1.f, 0.f);
	case 2: return Vector(0.f, 1.f);
	default: return Vector(0.f, -1.f);
	}
}


float PerlinNoise::Noise(float fx, float fy) {

	// сразу находим координаты левой верхней вершины квадрата
	int left = floor(fx);
	int top = floor(fy);

	// а теперь локальные координаты точки внутри квадрата
	float pointInQuadX = fx - left;
	float pointInQuadY = fy - top;

	// извлекаем градиентные векторы для всех вершин квадрата:
	Vector topLeftGrad = GetPseudoRandomGradientVector(left, top);
	Vector topRightGrad = GetPseudoRandomGradientVector(left + 1, top);
	Vector bottomLeftGrad = GetPseudoRandomGradientVector(left, top + 1);
	Vector bottomRightGrad = GetPseudoRandomGradientVector(left + 1, top + 1);

	// вектора от вершин квадрата до точки внутри квадрата:
	Vector distanceToTopLeft = Vector(pointInQuadX, pointInQuadY);
	Vector distanceToTopRight = Vector(pointInQuadX - 1, pointInQuadY);
	Vector distanceToBottomLeft = Vector(pointInQuadX, pointInQuadY - 1);
	Vector distanceToBottomRight = Vector(pointInQuadX - 1, pointInQuadY - 1);


	// считаем скалярные произведения между которыми будем интерполировать
	/*
	tx1--tx2
	|    |
	bx1--bx2
	*/
	float tx1 = Dot(distanceToTopLeft, topLeftGrad);
	float tx2 = Dot(distanceToTopRight, topRightGrad);
	float bx1 = Dot(distanceToBottomLeft, bottomLeftGrad);
	float bx2 = Dot(distanceToBottomRight, bottomRightGrad);

	// собственно, интерполяция:
	float tx = QunticCurveInterpolation(tx1, tx2, pointInQuadX);
	float bx = QunticCurveInterpolation(bx1, bx2, pointInQuadX);
	return QunticCurveInterpolation(tx, bx, pointInQuadY);
}

float PerlinNoise::perlinNoise2D(float fx, float fy) {
	float amplitude = 1.f;
	float max = 0.f;
	float result = 0.f;

	for (size_t i = numOctaves; i > 0; i--) {
		max += amplitude;
		result += Noise(fx, fy) * amplitude;
		amplitude *= persistence;
		fx *= 2;
		fy *= 2;
	}
	return result / max;
}
